#!/bin/bash

urlencode() {
    # urlencode <string>

    old_lc_collate=$LC_COLLATE
    LC_COLLATE=C

    local length="${#1}"
    for (( i = 0; i < length; i++ )); do
        local c="${1:$i:1}"
        case $c in
            [a-zA-Z0-9.~_-]) printf '%s' "$c" ;;
            *) printf '%%%02X' "'$c" ;;
        esac
    done

    LC_COLLATE=$old_lc_collate
}

fetch() {
	local pmid=$1
	local outputFile=$2
	
	if efetch -db pubmed -id "$pmid" -format abstract > "$outputFile"
	then
		echo "[OK] $pmid"
		echo
	else
		echo "[NOK] $pmid"
		exit 1
	fi
}


if [ ! "$1" ];then
   echo ERROR - You must provide query string in argument
   exit 1
fi

QUERY_STRING="$1"
QUERY_STRING_AS_URL=$(urlencode "$QUERY_STRING")

PMID_FILE="/tmp/pmids-$QUERY_STRING_AS_URL"
OUTPUT_DIRECTORY="$QUERY_STRING_AS_URL"

if ! [ -e "$PMID_FILE" ] || ! [ -s "$PMID_FILE" ]
then
	echo
	echo Retrieve pub med ids
	echo
	if ! esearch -db pubmed -query "$QUERY_STRING"| efetch -format uid > "$PMID_FILE"
	then
		echo
		echo Failed to retrieve pub med ids
		echo
		exit $?
	fi
else 
	echo
	echo Pub med ids already downloaded
	echo
fi

mkdir -p "$OUTPUT_DIRECTORY"

cat "$PMID_FILE" | while read -r pmid
do
	if [ -e "$OUTPUT_DIRECTORY/$pmid" ] 
	then
        if ! [ -s "$OUTPUT_DIRECTORY/$pmid" ]
		then
			echo
            echo file "$pmid" empty
			if ! fetch "$pmid" "$OUTPUT_DIRECTORY/$pmid"
			then
				exit 1
			fi
		else
			echo "[OK] $pmid"
		fi
	else
		if ! fetch "$pmid" "$OUTPUT_DIRECTORY/$pmid"
		then
			exit 1
		fi
	fi
done
